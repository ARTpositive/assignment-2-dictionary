%define ABI_CONSTANT 16
%define MAX_LENGTH_STRING 256		; 255 chars + terminator
%define POINTER_SIZE_IN_BYTES 8
%define NOT_FOUND_ERROR_CODE 1
%define TOO_LONG_ERROR_CODE 2

%include "lib.inc"
%include "dict.inc"

%include "words.inc"

section .rodata
not_found_error_message: db "key not found", 0
too_long_error_message: db "key is too long", 0

section .bss
input_string: resb 256

section .text
global _start
_start:
	sub rsp, ABI_CONSTANT
	mov [rsp], r13

	mov r13, first		

	mov rdi, input_string
	mov rsi, MAX_LENGTH_STRING
	call read_line	; -> rax, rdx
	test rax, rax
	js .too_long
	
	mov rdi, input_string
	mov rsi, r13
	call find_word	; -> rax
	test rax, rax
	jz .not_found
	
	jmp .good
	
	.too_long:
		mov rdi, too_long_error_message
		call print_error
		mov rdi, TOO_LONG_ERROR_CODE
		jmp .end
	
	.not_found:
		mov rdi, not_found_error_message
		call print_error
		mov rdi, NOT_FOUND_ERROR_CODE
		jmp .end
		
	.good:
		mov r13, rax
		add r13, POINTER_SIZE_IN_BYTES
		mov rdi, r13
		call string_length				
		add r13, rax	
		inc r13	
						
		mov rdi, r13
		call print_string				
		
		xor rdi, rdi
		
	.end:
		mov r13, [rsp]
		add r13, ABI_CONSTANT
		call exit
		
