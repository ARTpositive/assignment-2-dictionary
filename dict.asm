global find_word
section .text

%include "lib.inc"

; Принимает указатель на нуль-терминированную строку 
; (указатель на ключ или псевдо-ключ) (rdi)
; и указатель на начало словаря (rsi).
; Проходит по всему словарю в поисках подходящего ключа.  
; Если подходящее вхождение найдено, вернёт (rax) адрес начала 
; вхождения в словарь (не значения), иначе вернёт 0.
find_word:
	push r12
	push r13
	mov r12, rdi 				; r12 - find string
	mov r13, rsi				; r13 - cur key in dictionary
	xor eax, eax
	
	.handle_key:
		mov rdi, r12
		lea rsi, [r13 + 8]
		call string_equals		
		test rax, rax			; found -> .good
		jnz .good
		
		cmp qword [r13], 0		
		jz .bad				; not found -> .bad

		mov r13, [r13]			; next word
	
		jmp .handle_key
		
	.good:
		mov rax, r13
		jmp .end	
	.bad:
		xor eax, eax
	.end:
		pop r13
		pop r12
		ret
