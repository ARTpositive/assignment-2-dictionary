%define key_id 0
; %1 - ключ, %2 - метка
%macro colon 2
  %ifstr %1                  
    %ifid %2
    	%2: dq key_id
            db %1, 0
    	%define key_id %2
    %else
      %error "Invalid label"
    %endif 
  %else
    %error "Key must be a string"
  %endif
%endmacro

