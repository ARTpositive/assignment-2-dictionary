ASM=nasm
FLAGS=-felf64 -g

build: lib.o dict.o main.o
	ld -o build dict.o lib.o main.o

lib.o: lib.asm
	$(ASM) $(FLAGS) -o lib.o lib.asm

dict.o: dict.asm lib.o
	$(ASM) $(FLAGS) -o dict.o dict.asm

main.o: main.asm dict.o
	$(ASM) $(FLAGS) -o main.o main.asm

.PHONY: test
test: build
	python3 test.py

.PHONY: clear
clear:
	rm *.o
